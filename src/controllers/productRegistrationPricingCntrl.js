import IdentityServiceSdk from 'identity-service-sdk';
import SessionManager from 'session-manager';
import PartnerSaleRegistrationServiceSdk, {
    PartnerCommercialSaleRegSynopsisView,
    PartnerCommercialSaleRegDraftView,
    UpdatePartnerCommercialSaleRegDraftReq
}from 'partner-sale-registration-draft-service-sdk';

angular
    .module('productRegistrationPricingCntrl',['ui.bootstrap']);
export default class productRegistrationPricingCntrl{
    constructor($scope,$rootScope,$q, $uibModal,$location,sessionManager, identityServiceSdk,partnerSaleRegistrationServiceSdk) {
        var draftId=window.localStorage.getItem("draftId");

        var isFromReg= window.localStorage.getItem("isFromProdRegPage");
        var isFromEditPage= window.localStorage.getItem("isFromProdRegEditPage");

        console.log("isFromReg",isFromReg);
        if(isFromReg=="true") {
            $q(
                resolve =>
                    sessionManager
                        .getAccessToken()
                        .then(
                            accessToken =>
                                resolve(accessToken)
                        )
            ).then(
                accessToken => {
                    $scope.loader=true;
                    $q(
                        resolve =>
                            partnerSaleRegistrationServiceSdk
                                .getPartnerSaleRegistrationDraftWithDraftId(draftId, accessToken)
                                .then(
                                    response =>
                                        resolve(response)
                                )
                    ).then(PartnerCommercialSaleRegSynopsisView => {
                        $scope.loader=false;
                        console.log(' RESPONSE PartnerCommercialSaleRegSynopsisView Pricing screen::',PartnerCommercialSaleRegSynopsisView);
                        $scope.productsPricingList=PartnerCommercialSaleRegSynopsisView;
                        if(isFromEditPage=="true"){
                            console.log("BEF EDIT",$scope.productsPricingList);
                            angular.forEach($scope.productsPricingList.compositeLineItems,function(value,ind) {
                                console.log("VVV",value);
                                if(value.price==null) {
                                    value.isChecked=false;
                                } else {
                                    console.log("IN ELSE BLKKKK")
                                    value.isChecked=true;
                                }
                                /*angular.forEach(value.components,function(componentsObj,i){
                                    console.log("hhhh",componentsObj);
                                    if(value.price==null) {
                                        componentsObj.isChecked=false;
                                    } else {
                                        componentsObj.isChecked=true;
                                    }
                                })*/

                                $scope.productsPricingList.compositeLineItems[ind]=value;
                                console.log("AFT EDIT",$scope.productsPricingList);
                            });
                        }
                    });
                });
        } else {
            $scope.productsPricingList = JSON.parse(window.localStorage.getItem("prodPricingList"));
        }
        console.log("copied content",$scope.productsPricingList);
        $scope.composites={};
        $scope.checkedCompositePrice=function(priceData,index){
            console.log("CALLING &&&",priceData,"INDEX",index);
            var json = $scope.productsPricingList.compositeLineItems[index];
            console.log("json",json);
           /* if(json['isChecked'] !=null) {
                if(json['isChecked'] == true) {
                    json['isChecked'] = false;
                } else {
                    json['isChecked'] = true;
                }
            } else {
                json['isChecked'] = true;
            }*/
            $scope.productsPricingList.compositeLineItems[index] = json;
            $scope.composites= $scope.productsPricingList.compositeLineItems;

            /*if(json['isChecked']==true) {
                angular.forEach($scope.productsPricingList.compositeLineItems[index].components,function(val,key) {
                    val.price=null;
                    $scope.productsPricingList.compositeLineItems[index].components[key]=val;
                });
            }*/

        };
        $scope.productRegistrationPricing_Next=function(isValid){

            angular.forEach($scope.composites,function(v,k){
                if(v.isChecked==true){
                    angular.forEach(v.components,function(val,key){
                        val.price=null;
                    })
                }
                else{
                    v.price=null;
                }
            });
            $scope.submitted=true;
            if(isValid){

                /*angular.forEach($scope.productsPricingList.compositeLineItems,function(val,key) {
                 delete val["isChecked"];
                 $scope.productsPricingList.compositeLineItems[key]=val;
                 });*/
                console.log("update request",$scope.productsPricingList);
                ///for update
                $q(
                    resolve =>
                        sessionManager
                            .getAccessToken()
                            .then(
                                accessToken =>
                                    resolve(accessToken)
                            )
                ).then(
                    accessToken => {
                        $scope.loader=true;
                        $q(
                            resolve =>
                                partnerSaleRegistrationServiceSdk
                                    .updatePartnerCommercialSaleRegDraft($scope.productsPricingList,draftId, accessToken)
                                    .then(
                                        response =>
                                            resolve(response)
                                    )
                        ).then(PartnerCommercialSaleRegSynopsisView => {
                            $scope.loader=false;
                            console.log(' RESPONSE in UPDATE ::',PartnerCommercialSaleRegSynopsisView);
                            window.localStorage.setItem("reviewList",JSON.stringify(PartnerCommercialSaleRegSynopsisView.id));
                            window.localStorage.setItem("prodPricingList",JSON.stringify($scope.productsPricingList));

                            $scope.modalInstance = $uibModal.open({
                                scope:$scope,
                                template: '<div class="modal-header"> <h4 class="modal-title">Success !</h4></div>' +
                                '<div class="modal-body">Price added successfully </div>' +
                                '<div class="modal-footer">' +
                                '<button class="btn btn-primary" type="button" ng-click="pricing_confirm()">Ok</button></div>',
                                size:'sm',
                                backdrop : 'static'
                            });

                        });
                    });
            }



          //  window.localStorage.setItem("test",JSON.stringify($scope.productsPricingList));


        };
        $scope.pricing_confirm=function(){
            $location.path("/productRegistrationReview");
        };
        $scope.productRegistrationPricing_Previous=function(){
           window.localStorage.setItem("EditedDrafts",JSON.stringify($scope.productsPricingList.id));
            $location.path("/productRegistrationDraftEdit");
        }
        $scope.Back_Drafts=function(){
            $location.path("/");
        };
    }
};

productRegistrationPricingCntrl.$inject=[
    '$scope',
    '$rootScope',
    '$q',
    '$uibModal',
    '$location',
    'sessionManager',
    'identityServiceSdk',
    'partnerSaleRegistrationServiceSdk'
];